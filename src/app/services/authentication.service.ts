import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-type':'application/x-www-form-urlencoded' })
}; 

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  readonly rootUrl = 'http://51.254.98.35:8000';


  constructor(private http:HttpClient) { }

  login(userEmail, password){
    var data = "email="+userEmail+"&password="+password;
    var reqHeader = new HttpHeaders({'Content-type':'application/x-www-form-urlencoded', 'No-Auth':'True'});
    return this.http.post(this.rootUrl+'/api/user/login', data,{headers: reqHeader})
  }
 

  public getReporting(){
    var id = localStorage.getItem('userID');
    return this.http.get(this.rootUrl+"/api/reporting-by-bakeliste/"+id)
  }  
 
  public addReporting(app_name, technology, using_links, lieu, commentaire, status, tache, date){ 
    const reporting = {
      app_name : app_name,
      technology : technology,
      using_links : using_links,
      lieu : lieu,
      commentaire : commentaire,
      status : status,
      tache : tache,
      date : date
    }
    return this.http.post(this.rootUrl+'/api/reporting_bakelistes', reporting)
  }

  logout() {
    localStorage.removeItem('userToken');
  }
  
}
