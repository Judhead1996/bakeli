import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public contact = {
    nameComplet: "Khalifa Aboubacar Sy Sidibé",
    email: "inuyashakha96@gmail.com",
    tel: "781311030",
    logo: "assets/images/logo.png",
    bakeli: "assets/images/bakeli.png"
  }

  constructor() {}

}
