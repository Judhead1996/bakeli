import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.page.html',
  styleUrls: ['./reporting.page.scss'],
})
export class ReportingPage implements OnInit {

  public id:string;
  private dataReporting: Object;

  constructor(private authService: AuthenticationService, private http:HttpClient) { }

  ngOnInit() {
  }

  onLoadReporting() {
    this.authService.getReporting()
    .subscribe(data=>{
      this.dataReporting = data;    
    },err=>{
      console.log(err);
    });
    console.log("Ok")
  }

}
