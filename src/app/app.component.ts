import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Reporting',
      url: '/reporting',
      icon: 'md-albums'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    },
    {title: "Logout", url:"logout", icon: 'log-out'},
    {title: "Exit", url:"Exit", icon: 'power'},
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar, private authService:AuthenticationService, private router:Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  onMenuItem(m) {
    if(m.url == 'logout') {
      this.authService.logout();
      this.router.navigateByUrl("/login");
    }   
    else if(m.url=='Exit') {
      navigator['app'].exitApp();
    }
    else {
      this.router.navigateByUrl(m.url);
    }
  }
}
